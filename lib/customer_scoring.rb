require 'faraday'
require 'faraday_middleware'
require 'hashie'
require 'multi_json'

module CustomerScoring
  # This module is extended to all exception object raised
  # while querying Customer Scoring API
  ErrorMixin = Module.new
end

require "customer_scoring/version"
require "customer_scoring/response"
require "customer_scoring/client"
