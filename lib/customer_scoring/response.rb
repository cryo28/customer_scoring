module CustomerScoring
  # Customer Scoring API response container
  class Response < Hashie::Dash
    property :propensity
    property :ranking
  end
end
