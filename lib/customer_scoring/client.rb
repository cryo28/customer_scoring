module CustomerScoring # :nodoc:
  # LeapFrog Online Customer Scoring API Client
  # The client internally makes all requests by invoking Faraday and a few its middlewares:
  #
  #    1. Logger middleware logs request/response headers (@see {#initialize} option :logger)
  #    2. Retry middleware makes 2 attempts to repeat the failed request
  #    3. FollowRedirects middleware processes HTTP 301, 302, 303, and 307 redirects
  #
  # @example
  #     client = CustomerScoring.new('http://internal.leapfrogonline.com/customer_scoring')
  #     result = client.query(income: 135000, zipcode: 60047, age: 32)
  #     result # => #<CustomerScoring::Response ranking="C" propensity=0.26532>
  #
  class Client
    DEFAULT_BASE_URL = 'http://internal.leapfrogonline.com/customer_scoring'

    attr_reader :base_url, :options

    # Instantiate a client
    #
    # @param [#to_s] base_url API Endpoint url
    # @param [Hash] options
    # @option options [Logger] :logger Logger instance to log request/response headers
    def initialize(base_url = DEFAULT_BASE_URL, options = {})
      @base_url = base_url.to_s
      @options  = options
    end

    # Query the API with provided parameters
    #
    # The client uses all parameters as request query parameters as opposed to slicing only valid parameters
    #
    # @param [Hash] params
    # @option params [#to_s] :income
    # @option params [#to_s] :zipcode
    # @option params [#to_s] :age
    # @raise [CustomerScoring::ErrorMixin] on any standard error
    # @return [CustomerScoring::Response]
    def query(params = {})
      response = conn.get(base_url) do |req|
        req.params.merge!(params)
      end

      json_hash = MultiJson.load(response.body)
      Response.new(json_hash)
    rescue StandardError => exc
      exc.extend(ErrorMixin)
      raise(exc)
    end

    protected

    # @return [Faraday::Connection]
    def conn
      @conn ||= Faraday.new(url: base_url) do |faraday|
        faraday.response(:logger, options[:logger]) if options[:logger]
        faraday.request(:retry)
        faraday.use FaradayMiddleware::FollowRedirects
        faraday.adapter(Faraday.default_adapter)
      end
    end
  end
end
