require 'spec_helper'
require 'logger'

describe CustomerScoring::Client do
  let(:client_options) { {} }
  subject{ described_class.new(described_class::DEFAULT_BASE_URL, client_options) }

  describe "#query" do
    let(:params) do
      {
          income: 50000,
          zipcode: 60201,
          age: 35
      }
    end

    let(:fake_response) do
      {
          status: 200,
          headers: {'Content-Type' => 'application/json'},
          body: <<-JSON
          {
                        "propensity": 0.26532,
                        "ranking": "C"
                      }
          JSON
      }
    end

    let(:expected_result) { MultiJson.load(fake_response[:body]) }

    def stub_api_request
      stub_http_request(:get, described_class::DEFAULT_BASE_URL).with(query: params)
    end

    it "accepts query parameters, makes a query and parses response" do
      a_req = stub_api_request.to_return(fake_response)
      result = subject.query(params)
      result.should be_a(CustomerScoring::Response)
      result.should == expected_result
      a_req.should have_been_made
    end

    it "intercepts all exceptions and mixes-in CustomerScoring::ErrorMixin-marker" do
      stub_api_request.to_timeout
      expect{ subject.query(params) }.to raise_exception(CustomerScoring::ErrorMixin)
    end

    it "logs all requests if options[:logger] is available" do
      io = StringIO.new
      logger = Logger.new(io)
      logger.level = Logger::DEBUG
      client_options.merge!(logger: logger)
      stub_api_request.to_return(fake_response)
      subject.query(params)
      io.rewind

      logger_output = io.readlines
      logger_output.should have(4).items
      logger_output[0].should =~ /get #{Regexp.quote(described_class::DEFAULT_BASE_URL)}/
    end
  end
end
