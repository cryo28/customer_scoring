# -*- encoding : utf-8 -*-
require 'bundler'
Bundler.setup

GEM_ROOT = Pathname.new(File.expand_path('../..', __FILE__))

require 'simplecov'
SimpleCov.start do
  root GEM_ROOT
end

require 'rspec'
require 'webmock/rspec'
WebMock.disable_net_connect!


require 'customer_scoring'

RSpec.configure do |c|
end

