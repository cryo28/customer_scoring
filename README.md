# CustomerScoring

LeapFrog Online Customer Scoring API Client

## Installation

Add this line to your application's Gemfile:

    gem 'customer_scoring'

And then execute:

    $ bundle


## Usage

```ruby
client = CustomerScoring.new('http://internal.leapfrogonline.com/customer_scoring')
result = client.query(income: 135000, zipcode: 60047, age: 32)
result # => #<CustomerScoring::Response ranking="C" propensity=0.26532>
```

The client internally makes all requests by invoking Faraday and a few its middlewares:

    1. Logger middleware logs request/response headers (@see {#initialize} option :logger)
    2. Retry middleware makes 2 attempts to repeat the failed request
    3. FollowRedirects middleware processes HTTP 301, 302, 303, and 307 redirects

## Error handling

If retries fail (due to any error such as unknown DNS address, timeout etc) the client raises original
Faraday exception which can you should handle in the "external" code as appropriate.

To make intercepting just the client's errors easier it extends exception objects with ErrorMixin module:

```ruby
  begin
    client.query()  # something that raises any descendant of StandardError
  rescue CustomerScoring::ErrorMixin => exc
    # handle exc
  end
```

## Generate Docs

```
 yard
```

and look for them at doc/index.html

## Running tests

```
rake
```

or

```
guard
```

Coverage report is generated in coverage/index.html

## References

My [GitHub profile](https://github.com/cryo28)
